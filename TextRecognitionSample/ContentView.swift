//
//  ContentView.swift
//  TextRecognitionSample
//
//  Created by Lotino Ron Jefferson on 2021/07/26.
//

import SwiftUI
import Vision

struct ContentView: View {
    
    var words = ["subwaySign", "announcement", "barcode", "barcode2", "mcdonalds"]
    @State private var recognizedText: String = "文字認証の結果はここに表示されます"
    @State private var selectedWord: String = "subwaySign"
    
    var body: some View {
        NavigationView {
            VStack {
                ScrollView {
                    Image(uiImage: UIImage(named: "\(selectedWord)")!)
                        .resizable()
                        .scaledToFit()
                    ZStack {
                        RoundedRectangle(cornerRadius: 20, style: .continuous)
                            .fill(Color.gray.opacity(0.2))
                        
                        Text(recognizedText)
                            .padding()
                    }
                    .padding()
                }
                
                Spacer()
                
                HStack {
                    Button(action: {
                        self.selectedWord = words.randomElement()!
                    }) {
                        Text("画像更新")
                    }
                    .padding()
                    .foregroundColor(.white)
                    .background(Capsule().fill(Color.blue))
                    
                    Spacer()
                    
                    Button(action: {
                        let processImage = UIImage(named: "\(selectedWord)")!.cgImage
                        self.recognizedText = recognizeText(from: [processImage!])
                    }) {
                        Text("スキャン")
                    }
                    .padding()
                    .foregroundColor(.white)
                    .background(Capsule().fill(Color.blue))
                }
                .padding()
            }
            .navigationBarTitle("画像の文字を認証")
        }
    }
    
    fileprivate func recognizeText(from images: [CGImage]) -> String {
        var entireRecognizedText = ""
        let recognizeTextRequest = VNRecognizeTextRequest { (request, error) in
            guard error == nil else { return }
            
            guard let observations = request.results as? [VNRecognizedTextObservation] else { return }
            
            let maximumRecognitionCandidates = 1
            for observation in observations {
                guard let candidate = observation.topCandidates(maximumRecognitionCandidates).first else { continue }
                
                entireRecognizedText += "\(candidate.string)\n"
                
            }
        }
        recognizeTextRequest.recognitionLevel = .accurate
        
        for image in images {
            let requestHandler = VNImageRequestHandler(cgImage: image, options: [:])
            
            try? requestHandler.perform([recognizeTextRequest])
        }
        
        return entireRecognizedText
    }
}





// プレビュー
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
