//
//  TextRecognitionSampleApp.swift
//  TextRecognitionSample
//
//  Created by Lotino Ron Jefferson on 2021/07/26.
//

import SwiftUI

@main
struct TextRecognitionSampleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
